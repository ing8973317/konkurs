package pl.ing.onlinegame

import jakarta.inject.Singleton
import pl.ing.onlinegame.model.Clan
import pl.ing.onlinegame.model.Group
import pl.ing.onlinegame.model.Players

@Singleton
class GameService {

    fun calculate(players: Players): List<List<Clan>> {

        val sorted = players.clans
            .filter { it.numberOfPlayers <= players.groupCount }
            .sortedWith(compareByDescending<Clan> { it.points }.thenBy { it.numberOfPlayers })
            .toMutableList()

        val groups = mutableListOf<Group>()

        while (sorted.size > 0) {

            val first = sorted.removeFirst()
            var group = Group(listOf(first), players.groupCount - first.numberOfPlayers)
            var next = false

            while (group.places > 0 && sorted.size > 0 && !next) {
                val pos = sorted.indexOfFirst { it.numberOfPlayers <= group.places }
                if (pos >= 0) {
                    group = group.add(sorted.removeAt(pos))
                } else {
                    next = true
                }
            }

            groups.add(group)

        }

        return groups.map { it.clans }

    }

}
