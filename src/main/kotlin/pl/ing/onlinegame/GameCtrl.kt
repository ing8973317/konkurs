package pl.ing.onlinegame

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import pl.ing.onlinegame.model.Clan
import pl.ing.onlinegame.model.Players
import javax.validation.Valid


@Controller("/onlinegame")
open class GameCtrl(
    private val gameService: GameService
) {

    @Post("/calculate")
    open fun calculate(@Valid @Body players: Players): HttpResponse<List<List<Clan>>> {

        val groups = gameService.calculate(players)

        return HttpResponse.ok(groups)
    }


}

