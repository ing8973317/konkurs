package pl.ing.onlinegame.model

import io.micronaut.core.annotation.Introspected
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Introspected
data class Clan(

    @field:Min(1)
    @field:Max(1000)
    val numberOfPlayers: Int,

    @field:Min(1)
    @field:Max(1000000)
    val points: Int
)