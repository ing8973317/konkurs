package pl.ing.onlinegame.model

import io.micronaut.core.annotation.Introspected
import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Introspected
data class Players(

    @field:Min(1)
    @field:Max(1000)
    val groupCount: Int,

    @field:Valid
    val clans: List<Clan>
)