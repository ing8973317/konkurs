package pl.ing.onlinegame.model

data class Group(
    val clans: List<Clan>,
    val places: Int
) {
    fun add(clan: Clan): Group {
        return this.copy(clans = clans + listOf(clan), places = places - clan.numberOfPlayers)
    }
}