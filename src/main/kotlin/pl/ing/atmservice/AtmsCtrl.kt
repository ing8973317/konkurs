package pl.ing.atmservice

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import pl.ing.atmservice.model.ATM
import pl.ing.atmservice.model.Task
import javax.validation.Valid


@Controller("/atms")
open class AtmsCtrl(
    private val atmsService: AtmsService
) {

    @Post("/calculateOrder")
    open fun calculateOrder(@Valid @Body tasks: List<Task>): HttpResponse<List<ATM>> {

        val order = atmsService.calculateOrder(tasks)

        return HttpResponse.ok(order)
    }

}

