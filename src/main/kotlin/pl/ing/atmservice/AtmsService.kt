package pl.ing.atmservice

import jakarta.inject.Singleton
import pl.ing.atmservice.model.ATM
import pl.ing.atmservice.model.Task

@Singleton
class AtmsService {

    fun calculateOrder(tasks: List<Task>): List<ATM> {

        return tasks
            .sortedWith(compareBy({ it.region }, { it.requestType.priority }))
            .map { ATM(it.region, it.atmId) }
            .distinct()

    }
}