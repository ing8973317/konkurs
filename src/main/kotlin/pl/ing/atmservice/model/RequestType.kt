package pl.ing.atmservice.model

enum class RequestType(val priority: Int) {
    FAILURE_RESTART(0),
    PRIORITY(1),
    SIGNAL_LOW(2),
    STANDARD(3)
}