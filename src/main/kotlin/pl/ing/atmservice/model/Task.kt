package pl.ing.atmservice.model

import io.micronaut.core.annotation.Introspected
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Introspected
data class Task(

    @field:Min(1)
    @field:Max(9999)
    val region: Int,

    val requestType: RequestType,

    @field:Min(1)
    @field:Max(9999)
    val atmId: Int
)