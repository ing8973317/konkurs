package pl.ing.atmservice.model

data class ATM(
    val region: Int,
    val atmId: Int
)