package pl.ing.transactions

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import pl.ing.transactions.model.Account
import pl.ing.transactions.model.Transaction
import javax.validation.Valid


@Controller("/transactions")
open class TransactionsCtrl(
    private val transactionsService: TransactionsService
) {

    @Post("/report")
    open fun report(@Valid @Body transactions: List<Transaction>): HttpResponse<List<Account>> {

        val report = transactionsService.report(transactions)

        return HttpResponse.ok(report)
    }

}

