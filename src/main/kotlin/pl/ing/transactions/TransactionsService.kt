package pl.ing.transactions

import jakarta.inject.Singleton
import pl.ing.transactions.model.Account
import pl.ing.transactions.model.Transaction

@Singleton
class TransactionsService {

    fun report(transactions: List<Transaction>): List<Account> {

        return transactions.fold(mutableMapOf<String, Account>()) { acc, t ->

            acc.computeIfPresent(t.creditAccount) { _, account ->
                account.credit(t)
            }

            acc.computeIfAbsent(t.creditAccount) { _ ->
                Account.fromCredit(t)
            }

            acc.computeIfPresent(t.debitAccount) { _, account ->
                account.debit(t)
            }

            acc.computeIfAbsent(t.debitAccount) { _ ->
                Account.fromDebit(t)
            }

            acc
        }
            .values
            .toList()
            .sortedBy { it.account }
    }
}