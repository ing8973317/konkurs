package pl.ing.transactions.model

import io.micronaut.core.annotation.Introspected
import java.math.BigDecimal
import javax.validation.constraints.Pattern

@Introspected
data class Transaction(
    @field:Pattern(regexp = "[0-9]{26}")
    val debitAccount: String,

    @field:Pattern(regexp = "[0-9]{26}")
    val creditAccount: String,

    val amount: BigDecimal
)