package pl.ing.transactions.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.math.BigDecimal

data class Account(
    val account: String,

    val debitCount: Int,

    val creditCount: Int,

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_FLOAT)
    val balance: BigDecimal
) {
    fun debit(t: Transaction): Account {
        return this.copy(
            debitCount = this.debitCount + 1,
            balance = (this.balance - t.amount).setScale(2)
        )
    }

    fun credit(t: Transaction): Account {
        return this.copy(
            creditCount = this.creditCount + 1,
            balance = (this.balance + t.amount).setScale(2)
        )
    }

    companion object {

        fun fromCredit(t: Transaction): Account {
            return Account(t.creditAccount, 0, 1, t.amount.setScale(2))
        }

        fun fromDebit(t: Transaction): Account {
            return Account(t.debitAccount, 1, 0, -t.amount.setScale(2))
        }
    }

}