package pl.ing

import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test
import java.io.File

@MicronautTest
class TransactionsTest {

    private val request = File("./src/test/resources/transactions_req_1.json")

    private val expected = File("./src/test/resources/transactions_res_1.json")
        .readText()
        .replace(" ", "")
        .replace("\r", "")
        .replace("\n", "")

    @Test
    fun report(spec: RequestSpecification) {

        spec
            .given()
            .contentType(ContentType.JSON)
            .body(request)
            .`when`()
            .post("/transactions/report")
            .then().statusCode(200)
            .body(CoreMatchers.equalTo(expected))

    }
}