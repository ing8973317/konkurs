package pl.ing

import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test
import java.io.File

@MicronautTest
class AtmServiceTest {

    private val request1 = File("./src/test/resources/atmservice_req_1.json")

    private val expected1 = File("./src/test/resources/atmservice_res_1.json")
        .readText()
        .replace(" ", "")
        .replace("\r", "")
        .replace("\n", "")

    private val request2 = File("./src/test/resources/atmservice_req_2.json")

    private val expected2 = File("./src/test/resources/atmservice_res_2.json")
        .readText()
        .replace(" ", "")
        .replace("\r", "")
        .replace("\n", "")

    @Test
    fun calculate1(spec: RequestSpecification) {

        spec
            .given()
            .contentType(ContentType.JSON)
            .body(request1)
            .`when`()
            .post("/atms/calculateOrder")
            .then().statusCode(200)
            .body(CoreMatchers.equalTo(expected1))

    }
    @Test
    fun calculate2(spec: RequestSpecification) {

        spec
            .given()
            .contentType(ContentType.JSON)
            .body(request2)
            .`when`()
            .post("/atms/calculateOrder")
            .then().statusCode(200)
            .body(CoreMatchers.equalTo(expected2))

    }
}