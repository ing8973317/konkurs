from locust import HttpUser, task

class MyLocust(HttpUser):
    min_wait = 80
    max_wait = 120

    @task(1)
    def task1(self):
        self.client.post("/atms/calculateOrder",json=
        [
          {
            "region": 4,
            "requestType": "STANDARD",
            "atmId": 1
          },
          {
            "region": 1,
            "requestType": "STANDARD",
            "atmId": 1
          },
          {
            "region": 2,
            "requestType": "STANDARD",
            "atmId": 1
          },
          {
            "region": 3,
            "requestType": "PRIORITY",
            "atmId": 2
          },
          {
            "region": 3,
            "requestType": "STANDARD",
            "atmId": 1
          },
          {
            "region": 2,
            "requestType": "SIGNAL_LOW",
            "atmId": 1
          },
          {
            "region": 5,
            "requestType": "STANDARD",
            "atmId": 1
          },
          {
            "region": 5,
            "requestType": "FAILURE_RESTART",
            "atmId": 1
          }
        ]
        )

    @task(1)
    def task2(self):
        self.client.post("/onlinegame/calculate", json=
        {
          "groupCount": 6,
          "clans": [
            {
              "numberOfPlayers": 4,
              "points": 50
            },
            {
              "numberOfPlayers": 2,
              "points": 70
            },
            {
              "numberOfPlayers": 6,
              "points": 60
            },
            {
              "numberOfPlayers": 1,
              "points": 15
            },
            {
              "numberOfPlayers": 5,
              "points": 40
            },
            {
              "numberOfPlayers": 3,
              "points": 45
            },
            {
              "numberOfPlayers": 1,
              "points": 12
            },
            {
              "numberOfPlayers": 4,
              "points": 40
            }
          ]
        }
        )

    @task(1)
    def task3(self):
        self.client.post("/transactions/report", json=
        [
          {
            "debitAccount": "32309111922661937852684864",
            "creditAccount": "06105023389842834748547303",
            "amount": 10.90
          },
          {
            "debitAccount": "31074318698137062235845814",
            "creditAccount": "66105036543749403346524547",
            "amount": 200.90
          },
          {
            "debitAccount": "66105036543749403346524547",
            "creditAccount": "32309111922661937852684864",
            "amount": 50.10
          }
        ]
        )
