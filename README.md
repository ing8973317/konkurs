## Build

The execution of the 'build.sh' script will create a jar file in the 'build/libs' directory.

```
./build.sh
```

If you want to define a specific JDK, uncomment the line in the script 'build.sh'.

```
#JAVA_HOME=<jdk_path>
```

---

## Run

The execution of the 'run.sh' script will start the application on port 8080.

```
./build.sh
```

If you want to define a specific JDK, uncomment the lines below in the script 'run.sh'.

```
#export JAVA_HOME=<jdk_path>
#export PATH=$JAVA_HOME/bin:$PATH
```

---

## SAST Report

GitLab SAST
```gl-sast-report.json```

---

## Licences

Gradle License Report
```licences.json```