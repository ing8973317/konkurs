FROM gradle:8.0.1-jdk17-alpine as build

RUN mkdir /build
WORKDIR /build
COPY . .

RUN gradle assembleShadowDist

FROM openjdk:17-alpine

RUN mkdir /app
WORKDIR /app
COPY --from=build /build/build/libs/*.jar .

CMD java -Xmx2g -jar konkurs-0.1-all.jar

